%global desc \
This package intends to provides a compatibility layer for Python between its \
two version release. The future and past packages are both provides for backports \
and forwards, in which you are able to use a single, clean codebase to run under \
Python3 environmets easily. With also providing futurize and pasteurize scripts, \
you can convert you Python code to support both version.

%{!?python3_shortver: %global python3_shortver %(%{__python3} -c 'import sys; print(str(sys.version_info.major) + "." + str(sys.version_info.minor))')}

Name:           future
Version:        1.0.0
Release:        2
Summary:        Missing compatibility layer on Python 3
License:        MIT
URL:            http://python-future.org/
Source0:        %{pypi_source future}
# https://github.com/PythonCharmers/python-future/issues/165
Patch0:         future-skip_tests_with_connection_errors.patch
Patch1:         fix-test_ftp-failed.patch
BuildArch:      noarch

%description
%{desc}

%package        -n python3-future
Summary:        Missing compatibility layer on Python 3
%{?python_provide:%python_provide python3-future}
BuildRequires:  python3-devel python3-setuptools python3-numpy python3-requests python3-pytest

%description    -n python3-future
%{desc}

%prep
%autosetup -p1

%build
CFLAGS="%{optflags}" %{__python3} setup.py build --executable="%{__python3} -s"

%install
CFLAGS="%{optflags}" %{__python3} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
mv $RPM_BUILD_ROOT%{_bindir}/futurize $RPM_BUILD_ROOT%{_bindir}/python3-futurize
mv $RPM_BUILD_ROOT%{_bindir}/pasteurize $RPM_BUILD_ROOT%{_bindir}/python3-pasteurize
sed -i -e '/^#!\//, 1d' $RPM_BUILD_ROOT%{python3_sitelib}/future/backports/test/pystone.py

touch futurize-3 futurize-%{?python3_shortver}
install -p futurize-3 futurize-%{?python3_shortver} $RPM_BUILD_ROOT%{_bindir}
ln -sf %{_bindir}/python3-futurize $RPM_BUILD_ROOT%{_bindir}/futurize-3
ln -sf %{_bindir}/python3-futurize $RPM_BUILD_ROOT%{_bindir}/futurize-%{?python3_shortver}

touch pasteurize-3 pasteurize-%{?python3_shortver}
install -p pasteurize-3 pasteurize-%{?python3_shortver} $RPM_BUILD_ROOT%{_bindir}
ln -sf %{_bindir}/python3-pasteurize $RPM_BUILD_ROOT%{_bindir}/pasteurize-3
ln -sf %{_bindir}/python3-pasteurize $RPM_BUILD_ROOT%{_bindir}/pasteurize-%{?python3_shortver}

%check
PYTHONPATH=$PWD/build/lib py.test-3 -v

%files          -n python3-future
%{!?_licensedir:%global license %doc}
%license LICENSE.txt
%doc README.rst
%{_bindir}/futurize-3
%{_bindir}/futurize-%{?python3_shortver}
%{_bindir}/pasteurize-3
%{_bindir}/pasteurize-%{?python3_shortver}
%{_bindir}/python3-futurize
%{_bindir}/python3-pasteurize
%{python3_sitelib}/*

%changelog
* Thu Jan 23 2025 xu_ping <707078654@qq.com> - 1.0.0-2
- fix test_ftp failed due to python3 3.11.6 upgrade to 3.11.11

* Mon Oct 14 2024 yaoxin <yao_xin001@hoperun.com> - 1.0.0-1
- Update to 1.0.0:
  * Add support for Python 3.12
  * Small updates to the docs
  * Add SECURITY.md describing security policy
  * Fix pasteurize: NameError: name 'unicode' is not defined
  * Add setuptools to requirements for building docs
  * Make the fix_unpacking fixer more robust
  * Small improvements to shell scripts according to shellcheck

* Fri Jul 07 2023 Ge Wang <wang__ge@126.com> - 0.18.3-1
- Update to version 0.18.3

* Fri Mar 10 2023 yaoxin <yaoxin30@h-partners.com> - 0.18.2-2
- Fix CVE-2022-40899

* Wed Jul 14 2021 liksh <liks11@chinaunicom.cn> - 0.18.2-1
- update package from 0.16.0 to 0.18.2

* Wed Oct 21 2020 leiju <leiju4@huawei.com> - 0.16.0-12
- remove python2 subpackage

* Thu Jan 09 2020 lihao <lihao129@huawei.com> - 0.16.0-11
- Package Init

